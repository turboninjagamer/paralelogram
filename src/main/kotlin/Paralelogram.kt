import kotlin.math.sin

open class Paralelogram(
    val a: Double,
    val b: Double,
    val ugao: Double
) {
    fun visinaA():Double {
        return  b * sin(ugao)
    }

    fun visinaB(): Double {
        return a * sin(ugao)
    }

    fun obim(): Double {
        return 2 * (a+b)
    }

    fun povrsina(): Double {
        return a * visinaA()
    }

    fun tip(): String {
        when{
            a == b && ugao == 90.0 -> return "kvadrat"
            a != b && ugao == 90.0 -> return "pravougaonik"
            a == b && ugao != 90.0 -> return "romb"
            else -> return "romboid"
        }
    }
}