

fun main() {

    println("Unesite stranice paralelograma, i ugao između njih, razdvojene zarezom.")
    val input1 = readLine()?:""
    val args = input1.split(",")
    val a = args[0]
    val b = args[1]
    val ugao = args[2]

    val paralelogram = Paralelogram(a.toDouble(), b.toDouble(), ugao.toDouble())
    println("Vaš paralelogram je ${paralelogram.tip()}")
    println("ha = b * sin(ugao) = ${paralelogram.visinaA()}")
    println("hb = a * sin(ugao) = ${paralelogram.visinaB()}")
    println("O = 2 * (a+b) = ${paralelogram.obim()}")
    println("P = a * ha = ${paralelogram.povrsina()}")
}



